PineApple removal Robot

This is the code for the NVIDIA JETANK to be able to recognize and remove Pineapple off pizza

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Inside you are going to find the "regression_intereactive.ipynb" used for training and creating new models for the AI computer vision
Simply run the code on your JETANK and do as instructed on screen

Take pictures of what you want the JETANK to be able to see and click on the location on the picture where the object is

after a good amount of pictures (i used 400+) you can train it by writing in how many epocs to train (please note training the AI too much will make it overfit the model which means it will ONLY recognize the exact pictures you gave it and therefore wont find the obejct you are looking for in a read scenario).

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Inside the PineAppleDetection.ipynb You will find the code for my Pineapple picking robot.

The first few blocks in the notebook is setup for the Camera aswell as the robot itself.

This is then followed by a command section where all the premade commands will be run for you to check if everything works correctly

The last part loads the trained AI model of your choice and then gets to work on the pizza

Assuming the AI works properly at tracking the pineapple the robot should move into position when placed infront of a pizza. Find the pineapple piece and promptly lift it off the pizza

I have uploaded a few different trained models "RealPizza5Epoc.pth, RealPizza15Epoc.pth, RealPizza30Epoc.pth, RealPizza60Epoc.pth, RealPizza100Epoc.pth" which works "sometimes" also highly dependent on how different our pizza and pineapple aswell as surronding are

for future use i would recommend switching to the YOLO AI algorithm instead of the regression one so you are able to track multiple pineapple pieces aswell as the pizza itself so you wont have to have the robot drive onto your delicious pizza itself.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Assuming you have access to a JETANK you will already have everything you need for robot part of the project.


For more info about the regression computervision AI i would refer to the Nvidia Fundamentals video: https://www.youtube.com/watch?v=LMsUP-W-3FI

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

YOUTUBE LIVE DEMOSTRATION

"sorry for the quality my phone cant focus anymore"

https://www.youtube.com/watch?v=TND4tk_89To
